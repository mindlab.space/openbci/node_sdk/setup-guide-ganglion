# Connect to Ganglion Board and Log Samples

This example code is meant to connect to the first Ganglion Board it finds over BLE and then console log the sample data as it is received

## Running

First have your Ganglion Board powered and ready!

To run this code `cd` into the example directory and run:
```sh
npm start
```
