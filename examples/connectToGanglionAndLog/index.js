const Ganglion = require('@openbci/ganglion');

const ganglion = new Ganglion();

ganglion.once('ganglionFound', (peripheral) => {
  // Stop scanning once device was found
  ganglion.searchStop();

  ganglion.on('sample', (sample) => {
    console.log(`\nSample No: ${sample.sampleNumber}`);
    for (let i = 0, iLength = ganglion.numberOfChannels(); i < iLength; i += 1) {
      console.log(`Channel ${i + 1}: ${sample.channelData[i].toFixed(8)} Volts.`);
    }
  });

  // Wait for ganglion to become ready before starting data stream
  ganglion.once('ready', () => {
    ganglion.streamStart();
  });

  // attempt to connect to the Ganglion board
  ganglion.connect(peripheral)
    .then(() => {
      console.log(`Connected to ${peripheral.advertisement.localName}`);
    })
    .catch((err) => {
      console.error(err);
    });
});

// Start scanning for BLE devices
ganglion.searchStart()
  .catch((err) => {
    console.error(err);
  });