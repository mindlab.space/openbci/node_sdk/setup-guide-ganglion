# List Available Ganglion Boards

This example code is meant to search for currently broadcasting BLE Ganglion Boards. It will then print a list of boards it finds into the console.

It is a very simple example but is quite handy to see if you ahve your Bluetooth device working and your boards working.

## Running

First have your Ganglion Board powered and ready!

To run this code `cd` into the example directory and run:
```sh
npm start
```
