const Ganglion = require('@openbci/ganglion');

const ganglion = new Ganglion();

const availableDevices = [];

ganglion.on('ganglionFound', (peripheral) => {
  if (!availableDevices.find((device) => device.id === peripheral.id)) {
    availableDevices.push(peripheral);
    console.log(`Found ${peripheral.advertisement.localName}. Address: ${peripheral.address}. Connectable: ${peripheral.connectable}.`);
  }
});

// Start scanning for BLE devices
console.log('Searching...');
ganglion.searchStart()
  .catch((err) => {
    console.error(err);
  });