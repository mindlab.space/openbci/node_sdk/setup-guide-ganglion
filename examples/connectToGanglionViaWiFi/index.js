const WiFi = require('@openbci/wifi');

const wifi = new Wifi({
  debug: false,
  verbose: true,
  latency: 10000
});

wifi.on('sample', (sample) => {
  for (let i = 0; i < wifi.getNumberOfChannels(); i += 1) {
    console.log(`Channel ${i + 1}: ${sample.channelData[i].toFixed(8)} Volts`);
  }
});

wifi.searchToStream({
  sampleRate: 250, // for the ganglion board this can be set to 250 or 1600
  shieldName: 'OpenBCI-9DB6', // change this to the wifi name broadcasted by the wifi shield
  streamStart: true
})
  .catch(console.error);
