const Ganglion = require('@openbci/ganglion');
const WebSocket = require('ws');

/**
 * @description Will connect to the first ganglion board and stream it's samples to the WebSocket
 * server.
 * 
 * The default location for the WebSocket server is on the localhost. However, if you run the server
 * on a remote system you should update the `webSocketServerAddress` variable to reflect the servers
 * location
 */

const webSocketServerAddress = 'ws://localhost:8080/ganglion'; // set port and address of server
const heartbeatTime = 6000; // should be slightly higher than the server ping time

let heartbeatTimeout;

/**
 * Heartbeat function which will terminate the current connection if no ping message is returned
 * within the heartbeatTime variable. Once a ping message has been received the timer will reset.
 */
function heartbeat() {
  clearTimeout(heartbeatTimeout);

  heartbeatTimeout = setTimeout(() => {
    console.log('Terminating WebSocket connection due to no heartbeat');
    wsClient.terminate();
  }, heartbeatTime);
}


const ganglion = new Ganglion();
const wsClient = new WebSocket(webSocketServerAddress);

ganglion.once('ganglionFound', (peripheral) => {
  // Stop scanning once device was found
  ganglion.searchStop();

  ganglion.on('sample', (sample) => {
    try {
    const sampleBuffer = Buffer.from(JSON.stringify({
      id: peripheral.id,
      name: peripheral.advertisement.localName,
      sample
    }), 'utf8');
    wsClient.send(sampleBuffer);
  } catch(err) {
    console.error(err);
  }
  });

  // Wait for ganglion to become ready before starting data stream
  ganglion.once('ready', () => {
    ganglion.streamStart();
  });

  ganglion.once('close', () => {
    console.log('BLE disconnected from Ganglion Board');

    clearTimeout(heartbeatTimeout);
    wsClient.close();

    process.exit(0);
  });

  // attempt to connect to the Ganglion board
  ganglion.connect(peripheral)
    .then(() => {
      console.log(`BLE connected to ${peripheral.advertisement.localName}`);
    })
    .catch((err) => {
      console.error(err);
    });
});

wsClient.on('open', heartbeat);
wsClient.on('ping', heartbeat);

wsClient.on('close', () => {
  console.log('Disconnected from WebSocket Server');

  clearTimeout(heartbeatTimeout);

  // WebSocket not configured to reconnect so we exit application when disconnected
  process.exit(0);
});

wsClient.on('open', () => {
  console.log('Connected to WebSocket Server');

  ganglion.searchStart()
    .catch((err) => {
      console.error(err);
    });
});

/**
 * Process handlers to clean up the server
 */
process.on('SIGINT', () => {
  console.log('\nSIGINT Received. Shutting Down');

  clearTimeout(heartbeatTimeout);
  ganglion.close();
  wsClient.close();

  process.exit(0);
});

process.on('SIGTERM', () => {
  console.log('\nSIGTERM Received. Shutting Down');

  clearTimeout(heartbeatTimeout);
  ganglion.close();
  wsClient.close();

  process.exit(0);
});
