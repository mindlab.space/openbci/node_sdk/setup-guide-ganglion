const WebSocket = require('ws');

/**
 * @description Will generate random data and send to the WebSocket server. This can be used to test
 * the server without having a Ganglion Board available.
 * 
 * The default location for the WebSocket server is on the localhost. However, if you run the server
 * on a remote system you should update the `webSocketServerAddress` variable to reflect the servers
 * location
 */

const webSocketServerAddress = 'ws://localhost:8080/ganglion'; // set port and address of server
const heartbeatTime = 6000; // should be slightly higher than the server ping time

let heartbeatTimeout;

/**
 * Heartbeat function which will terminate the current connection if no ping message is returned
 * within the heartbeatTime variable. Once a ping message has been received the timer will reset.
 */
function heartbeat() {
  clearTimeout(heartbeatTimeout);

  heartbeatTimeout = setTimeout(() => {
    console.log('Terminating WebSocket connection due to no heartbeat');
    wsClient.terminate();
  }, heartbeatTime);
}


const wsClient = new WebSocket(webSocketServerAddress);

wsClient.on('open', heartbeat);
wsClient.on('ping', heartbeat);
wsClient.on('close', () => {
  console.log('Disconnected from WebSocket Server');
  clearTimeout(heartbeatTimeout);
  process.exit(0);
});

wsClient.on('open', () => {
  console.log('Connected to WebSocket Server');

  setInterval(() => {
    const sampleBuffer = Buffer.from(JSON.stringify({
      id: 'test',
      name: 'test-ABCD',
      sample: {
        sampleNumber: Math.floor(Math.random() * 200),
        channelData: [
          Math.random(),
          Math.random(),
          Math.random(),
          Math.random()
        ],
        accelData: [1,2,3],
        timeStamp: new Date()
      }
    }), 'utf8');
    wsClient.send(sampleBuffer);
  }, 5); // 5ms is 200Hz which is roughly the sample rate of a ganglion board
});
