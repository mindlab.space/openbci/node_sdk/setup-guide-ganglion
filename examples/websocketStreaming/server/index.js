const http = require('http');
const WebSocket = require('ws');

/**
 * @description This is the WebSocket server in which clients can connect and publish their sample
 * data to. This will only print the data into the console but would give a starting point to pipe
 * the data into cloud / remote system machine learning.
 */

const pingInterval = 5000;

const server = http.createServer();
const wsServer = new WebSocket.Server({ server });

// From WebSocket docs for heartbeat
function noop() {}

/**
 * Will update the isAlive state on a specific socket which should be called uppon receiving a pong
 * message from the client
 */
function heartbeat() {
  this.isAlive = true;
}

wsServer.on('connection', (socket) => {
  console.log('Socket connection established');
  socket.on('pong', heartbeat);

  socket.on('message', (message) => {
    const json = JSON.parse(message);

    console.log(`\nSample From: ${json.name}`);
    console.log(`Sample No: ${json.sample.sampleNumber}`);
    for (let i = 0, iLength = json.sample.channelData.length; i < iLength; i += 1) {
      console.log(`Channel ${i + 1}: ${json.sample.channelData[i].toFixed(8)} Volts.`);
    }
  });


  socket.on('close', () => {
    console.log('\nClient disconnected');
  });
});

server.listen(8080, () => {
  console.log('WebSocket Server now listning');
});

const interval = setInterval(() => {
  wsServer.clients.forEach((socket) => {
    if (socket.isAlive === false) {
      console.log('Terminating socket');
      return socket.terminate();
    }
    
    socket.isAlive = false;
    socket.ping(noop);
  });
}, pingInterval);

/**
 * Process handlers to clean up the server
 */
process.on('SIGINT', () => {
  console.log('\nSIGINT Received. Shutting Down');
  clearInterval(interval);
  server.close();
  wsServer.close();
  process.exit(0);
});

process.on('SIGTERM', () => {
  console.log('\nSIGTERM Received. Shutting Down');
  clearInterval(interval);
  server.close();
  wsServer.close();
  process.exit(0);
});
