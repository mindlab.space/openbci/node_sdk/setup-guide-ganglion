# WebSocket Streaming

This example shows a basic WebSocket server and client which streams the Ganglion Boards data to a remote server.

This have two seperate folders which by default are configured to run on the same machine however by changing the websocket server address would allow you to stream the data to a remote system.

There is also a random sample generator within the client whih allow you to test the remote WebSocket server by streaming randomised data to it.

## Running the example

### Server

The server can be started by `cd` into the server directory and running:

```sh
npm start
```

The server should be started before running any client code.

### Client

The client code has two options for running. If you have a ganglion board and want to stream live data then first make sure your Ganglion Board is on and ready then `cd` into the client directory and run:

```sh
npm start
```

If you want to stream randomised sample data then run

```sh
npm start:generator
```

## Information

If a client disconnects from the server, the server will continue to run and wait for a new client to connect. However,
if the server is stopped before the client is then all connected clients will disconnect (if using this example code).

If you start receiving "'state' is not a method of undefined" or "binding.node" errors it is likely that your node_modules folder has not built correctly so you should run npm install again to fix this. If you are still having issues and are running linux you may need to run the application as sudo or use setcap detailed in the main repository readme.