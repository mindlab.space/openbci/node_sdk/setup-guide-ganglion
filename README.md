# Setup Guide Node.JS SDK for Ganglion Boards

This guide will contain information on how to setup and use the OpenBCI Node.JS SDK from our experimentation within the MindLab space and from the official documentation (where most of the info in this guide came from) can be found at the following links:

* [OpenBCI/OpenBCI_NodeJS](https://github.com/OpenBCI/OpenBCI_NodeJS) (GitHub repository)
* [OpenBCI/OpenBCI_NodeJS_Ganglion](https://github.com/OpenBCI/OpenBCI_NodeJS_Ganglion) (GitHub repository)
* [Noble](https://www.npmjs.com/package/noble) (NPM Package)

Most of the experimentation in this guide was done on an Ubuntu 18.04 machine so will likely contain more information regarding this style of system. However, feel free to add more information for other systems too!

## Prerequisites 

### Node.JS

Currently the OpenBCI Node.JS SDK only supports between Node 6 and Node 9 when connecting using Bluetooth.

[Node 8](https://nodejs.org/dist/latest-v8.x/) is the latest LTS still under maintenance within that range so is a good choice but feel free to use a different node version! The current status of Node versions can be on the [release page](https://github.com/nodejs/Release)

[NVM](https://github.com/nvm-sh/nvm) (Node Version Manager) is a good choice for installing multiple versions of node (especially older ones) as it allows you to switch between different versions through the cli. 

If you are planning on using WiFi to connect to your ganglion boards you are not limited to versions 6-9 in Node.JS and can make use of newer language features in Node.JS 10+. The latest downloads can be found at [Node.JS Downloads](https://nodejs.org/en/download/). However, I would still reccommend using NVM if your system supports it.

### Python 2.7

[Python 2.7](https://www.python.org/downloads/) is needed even though this is a Node.JS SDK so dont forget to install it! 

### Hardware

* BLE112 Bluetooth dongle (If connecting via bluetooth)
* [WiFi Shield](https://shop.openbci.com/collections/frontpage/products/wifi-shield?variant=44534009550) (If connecting via WiFi)
* [OpenBCI Ganglion Board](https://shop.openbci.com/products/pre-order-ganglion-board) (This is a requirement for ganglion examples)
* [OpenBCI Cyton Board](https://shop.openbci.com/collections/frontpage/products/cyton-biosensing-board-8-channel?variant=38958638542) (This can replace the ganglion board as long as you use the Cyton libraries in place of the Ganglion ones in the examples)


### System packages

There are some platform specific requirements which need to be met in order to be able to use the Node.JS SDK which are as follows:

#### MacOS

* [XCode](https://itunes.apple.com/ca/app/xcode/id497799835?mt=12) nice and easy

#### Linux

- Kernel version 3.6 or above is needed
- `libbluetooth-dev` from [nobel](https://www.npmjs.com/package/noble)
- `libudev-dev`

##### Ubuntu/Debian

```sh
sudo apt update
sudo apt install bluetooth bluez libbluetooth-dev libudev-dev
```

##### Fedora/RPM

```sh
sudo yum install bluez bluez-libs bluez-libs-devel
```

##### Other Info

When running on Linux you will need sudo permissions to be able to connect and use the Bluetooth adaptor. This can be done in a couple of ways. The first is to run your application with the sudo command e.g. `sudo yarn start` or `sudo npm start`. The second option involves allowing the node binary itself to have sudo privliages which allow you to run it without the `sudo` command.

If you are running node with the sudo command then you will need to make sure that node is available under /usr/bin.

In order to stream data on Linux without sudo access, you may need to give the `node` binary privileges to start and stop BLE advertising.

```sh
sudo setcap cap_net_raw+eip $(eval readlink -f `which node`)
```

**Note:** this command requires `setcap` to be installed. Install it with the `libcap2-bin` package.

```sh
sudo apt install libcap2-bin
```

#### Windows 8+

- [node-gyp requirements for Windows](https://github.com/TooTallNate/node-gyp#installation)
  - Python 2.7
  - Visual Studio ([Express](https://www.visualstudio.com/en-us/products/visual-studio-express-vs.aspx))
- [node-bluetooth-hci-socket prerequisites](https://github.com/sandeepmistry/node-bluetooth-hci-socket#windows)
  - Compatible Bluetooth 4.0 USB adapter
  - [WinUSB](<https://msdn.microsoft.com/en-ca/library/windows/hardware/ff540196(v=vs.85).aspx>) driver setup for Bluetooth 4.0 USB adapter, using [Zadig tool](http://zadig.akeo.ie/)

See [@don](https://github.com/don)'s set up guide on [Bluetooth LE with Node.js and Noble on Windows](https://www.youtube.com/watch?v=mL9B8wuEdms).

## Examples!

There is some example code within this repository which will allow you to get started running the Node.JS SDK.

### Running an example

All of the examples will work after running the follwing commands in each example directory:

First change directory into the example folder then

install all of the example dependencies run:
```sh
npm install 
```

Every time afterwards to start the example run:
```sh
npm start
```

If you prefer to use yarn then the same commands apply just replace npm with yarn!

### Example Code

* [List Available BT Ganglion Boards](https://gitlab.com/mindlab.space/node_sdk/setup-guide-ganglion/examples/listAvailableGanglion/README.md) prints a list of all available Ganglion boards
* [Connect to Ganglion Board and Log](https://gitlab.com/mindlab.space/node_sdk/setup-guide-ganglion/examples/connectToGanglionAndLog/README.md) connects to the first Ganglion board it finds and prints all channels in the console
* [WebSocket Streaming](https://gitlab.com/mindlab.space/node_sdk/setup-guide-ganglion/examples/websocketStreaming/README.md) connects to the first ganglion board and streams its samples over a WebSocket connection. This can be used to stream sample data to a remote system.
* [Connect to Ganglion via WiFi](https://gitlab.com/mindlab.space/node_sdk/setup-guide-ganglion/examples/connectToGanglionViaWiFi) connects to a specific WiFi sheild and logs the data into a console

## Other information

- If you are connecting to the ganglion board via BLE112 (Bluetooth) you will be limited to ~200-250Hz sample rate, this is a limitation of how much data can be sent over the bluetooth connection. If you want to stream at a higher sample rate then you will need to connect via the WiFi sheild. This will give you a sample rate of 1.6kHz on the ganglion as long as you configure it too.

- The Bluetooth libraries are limited to Node.JS 6-9 which is caused by an incompatability in one of its dependencies. If you want to be able to make use of newer language features in Node.JS 10+ then you will have to use the WiFi library which does not have this version limitation.

- When initially running the Connect and Log example I noticed that when using the built in bluetooth module the noble package would return a console message stating that I need to turn on my bluetooth adaptor but would still connect and start streaming.

- If you have any other example code from working in the MindLab then please add it to this repository!
